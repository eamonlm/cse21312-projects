/**********************************************
* File: AVLTree.h
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*  
* AVL Tree Class
* Modified Class from https://users.cs.fiu.edu/~weiss/dsaa_c++4/code
**********************************************/

#ifndef AVL_TREE_H
#define AVL_TREE_H

#include "dsexceptions.h"
#include <algorithm>
#include <iostream> 
using namespace std;

template <typename T>
class AVLTree
{
  public:
    /********************************************
    * Function Name  : AVLTree
    * Pre-conditions :  
    * Post-conditions: none
    *  
    ********************************************/
    AVLTree( ) : root{ nullptr }
      { }
    
    /********************************************
    * Function Name  : AVLTree
    * Pre-conditions :  const AVLTree & rhs 
    * Post-conditions: none
    *  
    ********************************************/
    AVLTree( const AVLTree & rhs ) : root{ nullptr }
    {
        root = clone( rhs.root );
    }

    /********************************************
    * Function Name  : AVLTree
    * Pre-conditions :  AVLTree && rhs 
    * Post-conditions: none
    *  
    ********************************************/
    AVLTree( AVLTree && rhs ) : root{ rhs.root }
    {
        rhs.root = nullptr;
    }
    
    /********************************************
    * Function Name  : ~AVLTree
    * Pre-conditions :  
    * Post-conditions: none
    *  
    ********************************************/
    ~AVLTree( )
    {
        makeEmpty( );
    }

    /**
     * Deep copy.
     */
    /********************************************
    * Function Name  : operator=
    * Pre-conditions :  const AVLTree & rhs 
    * Post-conditions: AVLTree &
    *  
    ********************************************/
    AVLTree & operator=( const AVLTree & rhs )
    {
        AVLTree copy = rhs;
        std::swap( *this, copy );
        return *this;
    }
        
    /**
     * Move.
     */
    /********************************************
    * Function Name  : operator=
    * Pre-conditions :  AVLTree && rhs 
    * Post-conditions: AVLTree &
    *  
    ********************************************/
    AVLTree & operator=( AVLTree && rhs )
    {
        std::swap( root, rhs.root );
        
        return *this;
    }
    
    /**
     * Find the smallest item in the tree.
     * Throw UnderflowException if empty.
     */
    /********************************************
    * Function Name  : findMin
    * Pre-conditions :  
    * Post-conditions: const T &
    *  
    ********************************************/
    const T & findMin( ) const
    {
        if( isEmpty( ) )
            throw UnderflowException{ };
        return findMin( root )->element;
    }

    /**
     * Find the largest item in the tree.
     * Throw UnderflowException if empty.
     */
    /********************************************
    * Function Name  : findMax
    * Pre-conditions :  
    * Post-conditions: const T &
    *  
    ********************************************/
    const T & findMax( ) const
    {
        if( isEmpty( ) )
            throw UnderflowException{ };
        return findMax( root )->element;
    }

    /**
     * Returns true if x is found in the tree.
     */
    /********************************************
    * Function Name  : contains
    * Pre-conditions :  const T & x 
    * Post-conditions: bool
    *  
    ********************************************/
    bool contains( const T & x ) const
    {
        return contains( x, root );
    }

    /**
     * Test if the tree is logically empty.
     * Return true if empty, false otherwise.
     */
    /********************************************
    * Function Name  : isEmpty
    * Pre-conditions :  
    * Post-conditions: bool
    *  
    ********************************************/
    bool isEmpty( ) const
    {
        return root == nullptr;
    }

    /**
     * Print the tree contents in sorted order.
     */
    /********************************************
    * Function Name  : printTree
    * Pre-conditions :  
    * Post-conditions: none
    *  
    ********************************************/
    void printTree( ) const
    {
        if( isEmpty( ) )
            cout << "Empty tree" << endl;
        else
            printTree( root );
    }

    /**
     * Make the tree logically empty.
     */
    /********************************************
    * Function Name  : makeEmpty
    * Pre-conditions :  
    * Post-conditions: none
    *  
    ********************************************/
    void makeEmpty( )
    {
        makeEmpty( root );
    }

    /**
     * Insert x into the tree; duplicates are ignored.
     */
    /********************************************
    * Function Name  : insert
    * Pre-conditions :  const T & x 
    * Post-conditions: none
    *  
    ********************************************/
    void insert( const T & x )
    {
        insert( x, root );
    }
     
    /**
     * Insert x into the tree; duplicates are ignored.
     */
    /********************************************
    * Function Name  : insert
    * Pre-conditions :  T && x 
    * Post-conditions: none
    *  
    ********************************************/
    void insert( T && x )
    {
        insert( std::move( x ), root );
    }
     
    /**
     * Remove x from the tree. Nothing is done if x is not found.
     */
    /********************************************
    * Function Name  : remove
    * Pre-conditions :  const T & x 
    * Post-conditions: none
    *  
    ********************************************/
    void remove( const T & x )
    {
        remove( x, root );
    }

  private:
    struct AVLNode
    {
        T element;
        AVLNode   *left;
        AVLNode   *right;
        int       height;

        /********************************************
        * Function Name  : AVLNode
        * Pre-conditions :  const T & ele, AVLNode *lt, AVLNode *rt, int h = 0 
        * Post-conditions: none
        *  
        ********************************************/
        AVLNode( const T & ele, AVLNode *lt, AVLNode *rt, int h = 0 )
          : element{ ele }, left{ lt }, right{ rt }, height{ h } { }
        
        /********************************************
        * Function Name  : AVLNode
        * Pre-conditions :  T && ele, AVLNode *lt, AVLNode *rt, int h = 0 
        * Post-conditions: none
        *  
        ********************************************/
        AVLNode( T && ele, AVLNode *lt, AVLNode *rt, int h = 0 )
          : element{ std::move( ele ) }, left{ lt }, right{ rt }, height{ h } { }
    };

    AVLNode *root;


    /**
     * Internal method to insert into a subtree.
     * x is the item to insert.
     * t is the node that roots the subtree.
     * Set the new root of the subtree.
     */
/********************************************
* Function Name  : insert
* Pre-conditions :  const T & x, AVLNode * & t 
* Post-conditions: `    void
*  
********************************************/
    void insert( const T & x, AVLNode * & t )
    {
        if( t == nullptr )
            t = new AVLNode{ x, nullptr, nullptr };
        else if( x < t->element )
            insert( x, t->left );
        else if( t->element < x )
            insert( x, t->right );
        
        balance( t );
    }

    /**
     * Internal method to insert into a subtree.
     * x is the item to insert.
     * t is the node that roots the subtree.
     * Set the new root of the subtree.
     */
    /********************************************
    * Function Name  : insert
    * Pre-conditions :  T && x, AVLNode * & t 
    * Post-conditions: none
    *  
    ********************************************/
    void insert( T && x, AVLNode * & t )
    {
        if( t == nullptr )
            t = new AVLNode{ std::move( x ), nullptr, nullptr };
        else if( x < t->element )
            insert( std::move( x ), t->left );
        else if( t->element < x )
            insert( std::move( x ), t->right );
        
        balance( t );
    }
     
    /**
     * Internal method to remove from a subtree.
     * x is the item to remove.
     * t is the node that roots the subtree.
     * Set the new root of the subtree.
     */
    /********************************************
    * Function Name  : remove
    * Pre-conditions :  const T & x, AVLNode * & t 
    * Post-conditions: none
    *  
    ********************************************/
    void remove( const T & x, AVLNode * & t )
    {
        if( t == nullptr )
            return;   // Item not found; do nothing
        
        if( x < t->element )
            remove( x, t->left );
        else if( t->element < x )
            remove( x, t->right );
        else if( t->left != nullptr && t->right != nullptr ) // Two children
        {
            t->element = findMin( t->right )->element;
            remove( t->element, t->right );
        }
        else
        {
            AVLNode *oldNode = t;
            t = ( t->left != nullptr ) ? t->left : t->right;
            delete oldNode;
        }
        
        balance( t );
    }
    
    static const int ALLOWED_IMBALANCE = 1;

    // Assume t is balanced or within one of being balanced
    /********************************************
    * Function Name  : balance
    * Pre-conditions :  AVLNode * & t 
    * Post-conditions: none
    *  
    ********************************************/
    void balance( AVLNode * & t )
    {
        if( t == nullptr )
            return;
        
        if( height( t->left ) - height( t->right ) > ALLOWED_IMBALANCE )
            if( height( t->left->left ) >= height( t->left->right ) )
                rotateWithLeftChild( t );
            else
                doubleWithLeftChild( t );
        else
        if( height( t->right ) - height( t->left ) > ALLOWED_IMBALANCE )
            if( height( t->right->right ) >= height( t->right->left ) )
                rotateWithRightChild( t );
            else
                doubleWithRightChild( t );
                
        t->height = max( height( t->left ), height( t->right ) ) + 1;
    }
    
    /**
     * Internal method to find the smallest item in a subtree t.
     * Return node containing the smallest item.
     */
    /********************************************
    * Function Name  : findMin
    * Pre-conditions :  AVLNode *t 
    * Post-conditions: AVLNode *
    *  
    ********************************************/
    AVLNode * findMin( AVLNode *t ) const
    {
        if( t == nullptr )
            return nullptr;
        if( t->left == nullptr )
            return t;
        return findMin( t->left );
    }

    /**
     * Internal method to find the largest item in a subtree t.
     * Return node containing the largest item.
     */
    /********************************************
    * Function Name  : findMax
    * Pre-conditions :  AVLNode *t 
    * Post-conditions: AVLNode *
    *  
    ********************************************/
    AVLNode * findMax( AVLNode *t ) const
    {
        if( t != nullptr )
            while( t->right != nullptr )
                t = t->right;
        return t;
    }


    /**
     * Internal method to test if an item is in a subtree.
     * x is item to search for.
     * t is the node that roots the tree.
     */
    /********************************************
    * Function Name  : contains
    * Pre-conditions :  const T & x, AVLNode *t 
    * Post-conditions: bool
    *  
    ********************************************/
    bool contains( const T & x, AVLNode *t ) const
    {
        if( t == nullptr )
            return false;
        else if( x < t->element )
            return contains( x, t->left );
        else if( t->element < x )
            return contains( x, t->right );
        else
            return true;    // Match
    }

    /**
     * Internal method to make subtree empty.
     */
    /********************************************
    * Function Name  : makeEmpty
    * Pre-conditions :  AVLNode * & t 
    * Post-conditions: none
    *  
    ********************************************/
    void makeEmpty( AVLNode * & t )
    {
        if( t != nullptr )
        {
            makeEmpty( t->left );
            makeEmpty( t->right );
            delete t;
        }
        t = nullptr;
    }

    /**
     * Internal method to print a subtree rooted at t in sorted order.
     */
    /********************************************
    * Function Name  : printTree
    * Pre-conditions :  AVLNode *t 
    * Post-conditions: none
    *  
    ********************************************/
    void printTree( AVLNode *t ) const
    {
        if( t != nullptr )
        {
            printTree( t->left );
            cout << t->element << endl;
            printTree( t->right );
        }
    }

    /**
     * Internal method to clone subtree.
     */
    /********************************************
    * Function Name  : clone
    * Pre-conditions :  AVLNode *t 
    * Post-conditions: AVLNode *
    *  
    ********************************************/
    AVLNode * clone( AVLNode *t ) const
    {
        if( t == nullptr )
            return nullptr;
        else
            return new AVLNode{ t->element, clone( t->left ), clone( t->right ), t->height };
    }
        // Avl manipulations
    /**
     * Return the height of node t or -1 if nullptr.
     */
    /********************************************
    * Function Name  : height
    * Pre-conditions :  AVLNode *t 
    * Post-conditions: int
    *  
    ********************************************/
    int height( AVLNode *t ) const
    {
        return t == nullptr ? -1 : t->height;
    }

    /********************************************
    * Function Name  : max
    * Pre-conditions :  int lhs, int rhs 
    * Post-conditions: int
    *  
    ********************************************/
    int max( int lhs, int rhs ) const
    {
        return lhs > rhs ? lhs : rhs;
    }

    /**
     * Rotate binary tree node with left child.
     * For AVL trees, this is a single rotation for case 1.
     * Update heights, then set new root.
     */
    /********************************************
    * Function Name  : rotateWithLeftChild
    * Pre-conditions :  AVLNode * & k2 
    * Post-conditions: none
    *  
    ********************************************/
    void rotateWithLeftChild( AVLNode * & k2 )
    {
        AVLNode *k1 = k2->left;
        k2->left = k1->right;
        k1->right = k2;
        k2->height = max( height( k2->left ), height( k2->right ) ) + 1;
        k1->height = max( height( k1->left ), k2->height ) + 1;
        k2 = k1;
    }

    /**
     * Rotate binary tree node with right child.
     * For AVL trees, this is a single rotation for case 4.
     * Update heights, then set new root.
     */
    /********************************************
    * Function Name  : rotateWithRightChild
    * Pre-conditions :  AVLNode * & k1 
    * Post-conditions: none
    *  
    ********************************************/
    void rotateWithRightChild( AVLNode * & k1 )
    {
        AVLNode *k2 = k1->right;
        k1->right = k2->left;
        k2->left = k1;
        k1->height = max( height( k1->left ), height( k1->right ) ) + 1;
        k2->height = max( height( k2->right ), k1->height ) + 1;
        k1 = k2;
    }

    /**
     * Double rotate binary tree node: first left child.
     * with its right child; then node k3 with new left child.
     * For AVL trees, this is a double rotation for case 2.
     * Update heights, then set new root.
     */
    /********************************************
    * Function Name  : doubleWithLeftChild
    * Pre-conditions :  AVLNode * & k3 
    * Post-conditions: none
    *  
    ********************************************/
    void doubleWithLeftChild( AVLNode * & k3 )
    {
        rotateWithRightChild( k3->left );
        rotateWithLeftChild( k3 );
    }

    /**
     * Double rotate binary tree node: first right child.
     * with its left child; then node k1 with new right child.
     * For AVL trees, this is a double rotation for case 3.
     * Update heights, then set new root.
     */
    /********************************************
    * Function Name  : doubleWithRightChild
    * Pre-conditions :  AVLNode * & k1 
    * Post-conditions: none
    *  
    ********************************************/
    void doubleWithRightChild( AVLNode * & k1 )
    {
        rotateWithLeftChild( k1->right );
        rotateWithRightChild( k1 );
    }
};

#endif
